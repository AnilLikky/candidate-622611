$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/features/VisaEligibilityCheckForm.feature");
formatter.feature({
  "name": "Visa eligibility criteria check",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Verify whether a visa is required to visit the UK",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@smoke"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I provide a nationality of \"Japan\"",
  "keyword": "Given "
});
formatter.match({
  "location": "StepDefs.provideNationality(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I continue to next step",
  "keyword": "And "
});
formatter.match({
  "location": "StepDefs.continueToNextStep()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I select the reason \"study\"",
  "keyword": "And "
});
formatter.match({
  "location": "StepDefs.selectReasonForVisit(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I continue to next step",
  "keyword": "And "
});
formatter.match({
  "location": "StepDefs.continueToNextStep()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I state I am intending to stay for \"more\" than 6 months",
  "keyword": "And "
});
formatter.match({
  "location": "StepDefs.selectStayDuration(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I submit the form",
  "keyword": "When "
});
formatter.match({
  "location": "StepDefs.submitForm()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I will be informed \"You do not need a visa to come to the UK\"",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefs.verifyMessage(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Verify whether a visa is required to visit the UK",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@smoke"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I provide a nationality of \"Japan\"",
  "keyword": "Given "
});
formatter.match({
  "location": "StepDefs.provideNationality(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I continue to next step",
  "keyword": "And "
});
formatter.match({
  "location": "StepDefs.continueToNextStep()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I select the reason \"tourism\"",
  "keyword": "And "
});
formatter.match({
  "location": "StepDefs.selectReasonForVisit(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I submit the form",
  "keyword": "When "
});
formatter.match({
  "location": "StepDefs.submitForm()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I will be informed \"You won’t need a visa to come to the UK\"",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefs.verifyMessage(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Verify whether a visa is required to visit the UK",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@smoke"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I provide a nationality of \"Russia\"",
  "keyword": "Given "
});
formatter.match({
  "location": "StepDefs.provideNationality(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I continue to next step",
  "keyword": "And "
});
formatter.match({
  "location": "StepDefs.continueToNextStep()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I select the reason \"tourism\"",
  "keyword": "And "
});
formatter.match({
  "location": "StepDefs.selectReasonForVisit(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I continue to next step",
  "keyword": "And "
});
formatter.match({
  "location": "StepDefs.continueToNextStep()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I state \"yes\" to travelling with or visiting either your partner or a family member",
  "keyword": "And "
});
formatter.match({
  "location": "StepDefs.statePurposeOfVisit(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I submit the form",
  "keyword": "When "
});
formatter.match({
  "location": "StepDefs.submitForm()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I will be informed \"You’ll need a visa to come to the UK\"",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefs.verifyMessage(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});