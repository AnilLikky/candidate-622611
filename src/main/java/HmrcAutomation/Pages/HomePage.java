package HmrcAutomation.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class HomePage extends BasePage {

    public void selectCountry(String countryName) {
        Select countryList = new Select(driver.findElement(By.cssSelector(".govuk-select")));
        countryList.selectByVisibleText(countryName);
    }

    public boolean isUserOnHomePage() {
        return driver.findElement(By.cssSelector(".govuk-width-container.smart_answer")).isDisplayed();
    }

    public boolean isUserOnFieldListPage() {
        return driver.findElement(By.cssSelector(".govuk-fieldset")).isDisplayed();
    }

    public void selectReason(String reason) {
        driver.findElement(By.cssSelector(".govuk-radios__item input[value=" + reason + "]")).click();
    }

    public void selectMoreThan6Months() {
        driver.findElement(By.cssSelector(".govuk-radios__item input[value='six_months_or_less']")).click();
    }

    public void selectLessThan6Months() {
        driver.findElement(By.cssSelector(".govuk-radios__item input[value='six_months_or_less']")).click();
    }

    public void submitForm() {
        driver.findElement(By.cssSelector(".gem-c-button--bottom-margin")).click();
    }

    public String verifyVisaRequirementsMessage() {
        return driver.findElement(By.cssSelector(".gem-c-heading")).getText();
    }

    public void selectPurposeOfVisit(String selection) {
        if (selection == "Yes") {
            driver.findElement(By.cssSelector(".govuk-radios__item input[value='yes']")).click();
        } else {
            driver.findElement(By.cssSelector(".govuk-radios__item input[value='no']")).click();
        }
    }
}
