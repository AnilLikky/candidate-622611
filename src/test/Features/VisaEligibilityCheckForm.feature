Feature: Visa eligibility criteria check

  @smoke
  Scenario: Verify whether a visa is required to visit the UK for study from Japan
    Given I provide a nationality of "Japan"
    And I continue to next step
    And I select the reason "study"
    And I continue to next step
    And I state I am intending to stay for "more" than 6 months
    When I submit the form
    Then I will be informed "You do not need a visa to come to the UK"

  @smoke
  Scenario: Verify whether a visa is required to visit the UK for tourism from Japan
    Given I provide a nationality of "Japan"
    And I continue to next step
    And I select the reason "tourism"
    When I submit the form
    Then I will be informed "You won’t need a visa to come to the UK"

  @smoke
  Scenario:Verify whether a visa is required to visit the UK for tourism from Russia
    Given I provide a nationality of "Russia"
    And I continue to next step
    And I select the reason "tourism"
    And I continue to next step
    And I state "yes" to travelling with or visiting either your partner or a family member
    When I submit the form
    Then I will be informed "You’ll need a visa to come to the UK"