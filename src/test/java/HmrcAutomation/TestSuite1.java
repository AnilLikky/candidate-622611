package HmrcAutomation;

import Utils.Utils;
import com.jayway.restassured.http.ContentType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import static com.jayway.restassured.RestAssured.given;

public class TestSuite1 {

    @Before
    public void setup() {
        //Test Setup
        Utils.setBaseURI(); //Setup Base URI
        Utils.setBasePath("/postcodes/"); //Setup Base Path
        Utils.setContentType(ContentType.JSON); //Setup Content Type
    }


    @Test
    public void validatePostCode() {
        given()
                .contentType(ContentType.JSON)
                .when()
                .get("SW1P4JA")
                .then()
                .statusCode(200);
    }

    @After
    public void afterTest() {
        //Reset Values
        Utils.resetBaseURI();
    }

}

