package HmrcAutomation;


import HmrcAutomation.Pages.HomePage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

public class StepDefs {

    HomePage homePage = new HomePage();

    @Given("I provide a nationality of {string}")
    public void provideNationality(String countryName) {
        Assert.assertTrue(homePage.isUserOnHomePage());
        homePage.selectCountry(countryName);
    }

    @Given("I continue to next step")
    public void continueToNextStep() {
      homePage.submitForm();
    }

    @Given("I select the reason {string}")
    public void selectReasonForVisit(String reason) {
        Assert.assertTrue(homePage.isUserOnFieldListPage());
        homePage.selectReason(reason);
    }

    @Given("I state I am intending to stay for {string} than 6 months")
    public void selectStayDuration(String duration) {
        switch (duration) {
            case "more":
                homePage.selectMoreThan6Months();
                break;
            case "less":
                homePage.selectLessThan6Months();
                break;
        }
    }

    @Given("I state {string} to travelling with or visiting either your partner or a family member")
    public void statePurposeOfVisit(String selection) {
        homePage.selectPurposeOfVisit(selection);
    }
    @When("I submit the form")
    public void submitForm() {
        homePage.submitForm();
    }

    @Then("I will be informed {string}")
    public void verifyMessage(String actualMessage) {
      Assert.assertEquals(homePage.verifyVisaRequirementsMessage(), actualMessage);
    }

}
