Adding Readme file 


A master branch is created with name candidate-622611:


A feature branch is created  for task 1

Created the BDD automation scripts for the following 3 scenarios:
Scenarios:

    Given I provide a nationality of Japan

And I select the reason “Study”
And I state I am intending to stay for more than 6 months
When I submit the form
Then I will be informed “I need a visa to study in the UK”

Given I provide a nationality of Japan
And I select the reason “Tourism”
When I submit the form
Then I will be informed “I won’t need a visa to study in the UK”

Given I provide a nationality of Russia
And I select the reason “Tourism”
And I state I am not travelling or visiting a partner or family
When I submit the form
Then I will be informed “I need a visa to come to the UK”


A Feature file "visaEligibilityCheckForm.feature" is created for the above scenarios.
This is located in src/test/feature 

The Subsequent Steps defintions java class "StepDefs.java" with all the relavant methods are for the feature file  is created.
This is located in Src/test/java location

As for every scenario, the base URL /website has to be accessed and subsequenlty the browser has to be closed after test completion.
So, a hooks.java classs is created with @before and @After tags respectively.

A test runnerer file "RunCucumberTest.java" is created that glues the feature and the step definition file.

The output of the test exection is stored in 2 file formats : html, json, xml in the target folder in the rezpective format folders.


A pull request is created for the above 
https://gitlab.com/AnilLikky/candidate-622611/-/tree/feature/visavalidation


Next, the APi Test using selenium is created .
AllApiTest, TestSutie1 is created for the API test
No Feature file is created this task as I struggled to created a BDD for this API. so instead directly created a selenium test in java class.







